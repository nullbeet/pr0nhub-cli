from bs4 import BeautifulSoup as bs
import requests
import os
from shutil import which
from pprint import pprint as print



def get_source(url):
    return bs(requests.get(url).content, 'html.parser')


def search(terms):
    search_prefix = 'https://www.pornhub.com/video/search?search='
    soup = get_source(search_prefix + terms.replace(' ', '+'))
    
    return get_videos(soup)


def get_related(vid_url):
    soup = get_source(vid_url)
    return get_videos(soup)


def get_videos(soup_obj, user=False):
    url_prefix = 'https://www.pornhub.com'

    index = 0
    videos = {}
    for li in soup_obj.find_all('li', class_='js-pop'):
        vid_info = li.find('span', class_='title').find('a')
        if vid_info['href'] != 'javascript:void(0)':
            vid_stats = li.find('div', class_='videoDetailsBlock')
            thumbnail_url = li.find('img')['data-thumb_url']

            videos[index] = {
                        'url': url_prefix + vid_info['href'],
                        'title': vid_info['title'],
                        'views': vid_stats.find('span').text,
                        'rating': vid_stats.find('div', class_='value').text,
                        'user': user_info.text,
                        'user_url': url_prefix + user_info['href'],
                        'thumbnail_url': thumbnail_url
                    }
            if not user:
                user_info = li.find('div', class_='usernameWrap').find('a')
                videos[index].update({'user': user_info.text, 'user_url': url_prefix + user_info['href']})
            index += 1

    return videos

:col
