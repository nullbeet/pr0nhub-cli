# pr0nhub-cli

### CLI for searching PornHub.com
#### **Major WIP as this is my first actual public project**
*PRs, suggestions, name-calling, and demoralizing comments welcome*

<br>

### TODO:
- [ ] Play urls with mpv, mplayer and vlc
- [ ] Add option to download video
- [x] Fetch video description/info
- [x] Fetch video thumbnails
- [x] Add option to view related videos
- [ ] Add option to browse video uploader submissions
- [ ] Fetch video previews
- [ ] Create Actual TUI
